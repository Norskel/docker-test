server {
    listen                  80;
    listen                  [::]:80;
    listen                  443 ssl http2;
    listen                  [::]:443 ssl http2;
    server_name             example.com;
    set                     $base /var/www/example.com;
    root                    $base/public;

    # SSL
    ssl_certificate         /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/example.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/example.com/chain.pem;

    # security
    include                 nginxconfig.io/security.conf;

    # logging
    access_log              /var/log/nginx/example.com.access.log;
    error_log               /var/log/nginx/example.com.error.log warn;

    # index.php
    index                   index.php;

    # index.php fallback
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    # additional config
    include nginxconfig.io/general.conf;
    include nginxconfig.io/letsencrypt.conf;

    # handle .php
    location ~ \.php$ {
        include nginxconfig.io/php_fastcgi.conf;
    }
}

# subdomains redirect
server {
    listen                  443 ssl http2;
    listen                  [::]:443 ssl http2;
    server_name             *.example.com;

    # SSL
    ssl_certificate         /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/example.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/example.com/chain.pem;
    return                  301 https://example.com$request_uri;
}