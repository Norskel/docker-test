FROM nginx:alpine

RUN rm /usr/share/nginx/html/*
COPY data/* /usr/share/nginx/html/
COPY conf/* /etc/nginx/

